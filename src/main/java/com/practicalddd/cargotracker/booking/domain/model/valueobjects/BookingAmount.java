package com.practicalddd.cargotracker.booking.domain.model.valueobjects;

import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 * Domain model representation of the Booking Amount for a new Cargo.
 * Contains the Booking Amount of the Cargo
 */
@Embeddable
public class BookingAmount {

    @Column(name = "booking_amount", updatable= false)
//    @Column(name = "booking_amount", unique = true, updatable= false)
    private Integer amount;

    public BookingAmount(){}

    public BookingAmount(Integer bookingAmount){this.amount = bookingAmount;}

    public void setBookingAmount(Integer bookingAmount){this.amount = bookingAmount;}

    public Integer getBookingAmount(){return this.amount;}
}
