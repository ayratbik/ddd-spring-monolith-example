package com.practicalddd.cargotracker;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.List;

@Component
public class SqlDataInitializer implements ApplicationRunner {

    @Autowired
    JdbcTemplate jdbcTemplate;

    @Override
    public void run(ApplicationArguments args) {

        List<Object[]> voyageData = Arrays.asList(
                new Object[]{3, "0100S"}, new Object[]{4, "0101S"}, new Object[]{5, "0102S"});

        jdbcTemplate.batchUpdate("insert into cargo.voyage (Id,voyage_number) values(?,?)", voyageData);

        List<Object[]> cMovementData = Arrays.asList(
                new Object[]{1355, "CNHGH", "CNHKG", 3, "2019-08-28", "2019-08-25"},
                new Object[]{1356, "JNTKO", "CNHGH", 4, "2019-09-10", "2019-09-01"},
                new Object[]{1357, "USNYC", "JNTKO", 5, "2019-09-25", "2019-09-15"});

        jdbcTemplate.batchUpdate("insert into cargo.carrier_movement " +
                "(Id,arrival_location_id,departure_location_id,voyage_id,arrival_date,departure_date) " +
                "values(?,?,?,?,?,?)", cMovementData);
    }

}
