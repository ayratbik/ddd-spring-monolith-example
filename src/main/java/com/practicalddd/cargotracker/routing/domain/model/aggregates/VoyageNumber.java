package com.practicalddd.cargotracker.routing.domain.model.aggregates;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@SuppressWarnings("unused")
@Embeddable
public class VoyageNumber {
    @SuppressWarnings("JpaDataSourceORMInspection")
    @Column(name="voyage_number")
    private String voyageNumberValue;
    public VoyageNumber(){}
    public VoyageNumber(String voyageNumberValue){this.voyageNumberValue = voyageNumberValue;}
    public String getVoyageNumberValue(){return this.voyageNumberValue;}
    public void setVoyageNumberValue(String voyageNumberValue){this.voyageNumberValue = voyageNumberValue;}
}
