-- CREATE USER IF NOT EXISTS "SA" SALT 'd216c5d716c0d6a7' HASH '609a94c3ff0be3e2342d699fd318007d98c7d8d414501ca457a48d8bc5a73b02' ADMIN;
CREATE SCHEMA IF NOT EXISTS "CARGO"; --AUTHORIZATION "SA";
-- CREATE SEQUENCE "CARGO"."SYSTEM_SEQUENCE_39218159_FC4C_4375_A43A_64F2B53FA586" START WITH 1;
-- CREATE SEQUENCE "CARGO"."SYSTEM_SEQUENCE_C0F2F453_978D_4A2D_8322_34E9893C9C50" START WITH 1;
-- CREATE SEQUENCE "CARGO"."SYSTEM_SEQUENCE_73E28431_9765_434B_944A_F1E32A95F2DC" START WITH 1;
-- CREATE SEQUENCE "CARGO"."HIBERNATE_SEQUENCE" START WITH 1;
-- CREATE SEQUENCE "CARGO"."SYSTEM_SEQUENCE_70859BC8_6D0E_42B5_BA15_0CC12EBA927F" START WITH 1;
-- CREATE SEQUENCE "CARGO"."SYSTEM_SEQUENCE_B32F85EC_CCFF_4BE4_A64B_833C6B1B9BCB" START WITH 1;
CREATE TABLE "CARGO"."CARGO"(
                                       "ID" BIGINT PRIMARY KEY,
--                                            DEFAULT NEXT VALUE FOR "CARGO"."SYSTEM_SEQUENCE_C0F2F453_978D_4A2D_8322_34E9893C9C50" NOT NULL NULL_TO_DEFAULT SEQUENCE "CARGO"."SYSTEM_SEQUENCE_C0F2F453_978D_4A2D_8322_34E9893C9C50",
                                       "BOOKING_AMOUNT" INTEGER,
                                       "BOOKING_ID" VARCHAR(255),
                                       "CURRENT_VOYAGE_NUMBER" VARCHAR(255),
                                       "LAST_HANDLING_EVENT_ID" INTEGER,
                                       "LAST_HANDLING_EVENT_LOCATION" VARCHAR(255),
                                       "LAST_HANDLING_EVENT_TYPE" VARCHAR(255),
                                       "LAST_HANDLING_EVENT_VOYAGE" VARCHAR(255),
                                       "LAST_KNOWN_LOCATION_ID" VARCHAR(255),
                                       "NEXT_EXPECTED_LOCATION_ID" VARCHAR(255),
                                       "NEXT_EXPECTED_HANDLING_EVENT_TYPE" VARCHAR(255),
                                       "NEXT_EXPECTED_VOYAGE_ID" VARCHAR(255),
                                       "ROUTING_STATUS" VARCHAR(255),
                                       "TRANSPORT_STATUS" VARCHAR(255),
                                       "ORIGIN_ID" VARCHAR(255),
                                       "SPEC_ARRIVAL_DEADLINE" DATE,
                                       "SPEC_DESTINATION_ID" VARCHAR(255),
                                       "SPEC_ORIGIN_ID" VARCHAR(255)
);
-- ALTER TABLE "CARGO"."CARGO" ADD CONSTRAINT "CARGO"."CONSTRAINT_3" PRIMARY KEY("ID");
-- 0 +/- SELECT COUNT(*) FROM CARGO.CARGO;
CREATE TABLE "CARGO"."CARRIER_MOVEMENT"(
                                                  "ID" BIGINT PRIMARY KEY,
--                                                       NOT NULL,
                                                  "ARRIVAL_DATE" DATE,
                                                  "ARRIVAL_LOCATION_ID" VARCHAR(255),
                                                  "DEPARTURE_DATE" DATE,
                                                  "DEPARTURE_LOCATION_ID" VARCHAR(255),
                                                  "VOYAGE_ID" BIGINT
);
-- ALTER TABLE "CARGO"."CARRIER_MOVEMENT" ADD CONSTRAINT "CARGO"."CONSTRAINT_5" PRIMARY KEY("ID");
-- 0 +/- SELECT COUNT(*) FROM CARGO.CARRIER_MOVEMENT;
CREATE TABLE "CARGO"."HANDLING_ACTIVITY"(
                                                   "ID" BIGINT PRIMARY KEY,
--                                                        DEFAULT NEXT VALUE FOR "CARGO"."SYSTEM_SEQUENCE_B32F85EC_CCFF_4BE4_A64B_833C6B1B9BCB" NOT NULL NULL_TO_DEFAULT SEQUENCE "CARGO"."SYSTEM_SEQUENCE_B32F85EC_CCFF_4BE4_A64B_833C6B1B9BCB",
                                                   "BOOKING_ID" VARCHAR(255),
                                                   "EVENT_COMPLETION_TIME" DATE,
                                                   "LOCATION" VARCHAR(255),
                                                   "EVENT_TYPE" VARCHAR(255),
                                                   "VOYAGE_NUMBER" VARCHAR(255)
);
-- ALTER TABLE "CARGO"."HANDLING_ACTIVITY" ADD CONSTRAINT "CARGO"."CONSTRAINT_4" PRIMARY KEY("ID");
-- 0 +/- SELECT COUNT(*) FROM CARGO.HANDLING_ACTIVITY;
CREATE TABLE "CARGO"."LEG"(
                                     "ID" BIGINT PRIMARY KEY,
--                                          DEFAULT NEXT VALUE FOR "CARGO"."SYSTEM_SEQUENCE_73E28431_9765_434B_944A_F1E32A95F2DC" NOT NULL NULL_TO_DEFAULT SEQUENCE "CARGO"."SYSTEM_SEQUENCE_73E28431_9765_434B_944A_F1E32A95F2DC",
                                     "LOAD_LOCATION_ID" VARCHAR(255),
                                     "LOAD_TIME" TIMESTAMP NOT NULL,
                                     "UNLOAD_LOCATION_ID" VARCHAR(255),
                                     "UNLOAD_TIME" TIMESTAMP NOT NULL,
                                     "VOYAGE_NUMBER" VARCHAR(255),
                                     "CARGO_ID" BIGINT
);
-- ALTER TABLE "CARGO"."LEG" ADD CONSTRAINT "CARGO"."CONSTRAINT_1" PRIMARY KEY("ID");
-- 0 +/- SELECT COUNT(*) FROM CARGO.LEG;
CREATE TABLE "CARGO"."TRACKING_ACTIVITY"(
                                                   "ID" BIGINT PRIMARY KEY,
--                                                        DEFAULT NEXT VALUE FOR "CARGO"."SYSTEM_SEQUENCE_70859BC8_6D0E_42B5_BA15_0CC12EBA927F" NOT NULL NULL_TO_DEFAULT SEQUENCE "CARGO"."SYSTEM_SEQUENCE_70859BC8_6D0E_42B5_BA15_0CC12EBA927F",
                                                   "BOOKING_ID" VARCHAR(255),
                                                   "TRACKING_NUMBER" VARCHAR(255)
);
-- ALTER TABLE "CARGO"."TRACKING_ACTIVITY" ADD CONSTRAINT "CARGO"."CONSTRAINT_A" PRIMARY KEY("ID");
-- 0 +/- SELECT COUNT(*) FROM CARGO.TRACKING_ACTIVITY;
CREATE TABLE "CARGO"."TRACKING_HANDLING_EVENTS"(
                                                          "ID" BIGINT PRIMARY KEY,
--                                                               DEFAULT NEXT VALUE FOR "CARGO"."SYSTEM_SEQUENCE_39218159_FC4C_4375_A43A_64F2B53FA586" NOT NULL NULL_TO_DEFAULT SEQUENCE "CARGO"."SYSTEM_SEQUENCE_39218159_FC4C_4375_A43A_64F2B53FA586",
                                                          "EVENT_TIME" DATE,
                                                          "EVENT_TYPE" VARCHAR(255),
                                                          "LOCATION_ID" VARCHAR(255),
                                                          "VOYAGE_NUMBER" VARCHAR(255),
                                                          "TRACKING_ID" BIGINT
);
-- ALTER TABLE "CARGO"."TRACKING_HANDLING_EVENTS" ADD CONSTRAINT "CARGO"."CONSTRAINT_AE" PRIMARY KEY("ID");
-- 0 +/- SELECT COUNT(*) FROM CARGO.TRACKING_HANDLING_EVENTS;
CREATE TABLE "CARGO"."VOYAGE"(
                                        "ID" BIGINT  PRIMARY KEY,
--                                             NOT NULL,
                                        "VOYAGE_NUMBER" VARCHAR(255)
);
-- ALTER TABLE "CARGO"."VOYAGE" ADD CONSTRAINT "CARGO"."CONSTRAINT_9" PRIMARY KEY("ID");
-- 0 +/- SELECT COUNT(*) FROM CARGO.VOYAGE;
-- ALTER TABLE "CARGO"."CARGO" ADD CONSTRAINT "CARGO"."UK_HLJRUTIWWIRE84G681Y6WJNCF" UNIQUE("BOOKING_AMOUNT");
ALTER TABLE "CARGO"."LEG" ADD CONSTRAINT "CARGO"."FKFYRFBXC9AVV0JOKLX7JCU6C9X" FOREIGN KEY("CARGO_ID") REFERENCES "CARGO"."CARGO"("ID") NOCHECK;
ALTER TABLE "CARGO"."CARRIER_MOVEMENT" ADD CONSTRAINT "CARGO"."FK19NU70GGLMXO40637VYKHB34J" FOREIGN KEY("VOYAGE_ID") REFERENCES "CARGO"."VOYAGE"("ID") NOCHECK;
ALTER TABLE "CARGO"."TRACKING_HANDLING_EVENTS" ADD CONSTRAINT "CARGO"."FKAL6SWI5X8QMI69UGNR597SOVT" FOREIGN KEY("TRACKING_ID") REFERENCES "CARGO"."TRACKING_ACTIVITY"("ID") NOCHECK;