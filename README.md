# [RU] Монолитная реализация приложения "Cargo Tracking"

Изменённый и исправленный проект из книги Apress "Practical DDD Enterprise Java", Chapter 3.
(https://github.com/Apress/practical-ddd-in-enterprise-java/tree/master/Chapter3/cargotracker)

Отличия от оригинала:

1. Стек изменён с JakartaEE на Spring(Boot) - теперь проект легко запустить без всяких предварительных настроек!
2. Система сборки изменена с maven на gradle (для удобства и красоты).
3. Добавлен проект функциональных тестов SoapUI.
4. Исправлены найденные ошибки и неточности.

Тестовый сценарий c запросами в test_requests.md.

Дальнейший план работ:

1. Сделать unit-тесты.
2. Доделать нереализованные функции:

* Booking: 
    * Booking has to listen to Handling and refresh fields in db table
    * Update statuses ROUTED and RECEIVED
  
* Delivery(Booking):
    * Route recalculation

# [EN] DDD Spring Monolithic Application #

The origin project implemented on Jakarta EE 8 platform was taken and remade to work with Spring Framework

The origin project code is from Apress "Practical DDD Enterprise Java", Chapter 3.
(https://github.com/Apress/practical-ddd-in-enterprise-java/tree/master/Chapter3/cargotracker)

### Benefits: ###

* no additional configuration and infrastructure are needed. Just use bootRun to start project!